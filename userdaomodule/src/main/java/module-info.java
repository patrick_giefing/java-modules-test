module es.rick.userdaomodule {
    requires es.rick.entitymodule;
    requires es.rick.daomodule;
    provides es.rick.dao.Dao with es.rick.userdao.UserDao, es.rick.userdao.UserDao2;
    exports es.rick.userdao;
}