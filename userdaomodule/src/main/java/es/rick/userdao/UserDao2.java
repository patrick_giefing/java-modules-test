package es.rick.userdao;

import es.rick.dao.Dao;
import es.rick.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserDao2 implements Dao<User> {
    private Map<Integer, User> users = Map.of(1, new User("pat"));

    public UserDao2() {
    }

    public UserDao2(Map<Integer, User> users) {
        this.users = users;
    }

    @Override
    public Optional<User> findById(int id) {
        return Optional.ofNullable(users.get(id));
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }
}