module es.rick.mainappmodule {
    requires es.rick.entitymodule;
    requires es.rick.userdaomodule;
    requires es.rick.daomodule;
    uses es.rick.dao.Dao;
}