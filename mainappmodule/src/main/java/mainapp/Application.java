package mainapp;

import es.rick.dao.Dao;
import es.rick.entity.User;
import es.rick.userdao.UserDao;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

public class Application {

    public static void main(String[] args) {
        ServiceLoader<Dao> daoServiceLoader = ServiceLoader.load(Dao.class);
        daoServiceLoader.forEach(dao -> dao.findAll().forEach(System.out::println));

        Map<Integer, User> users = new HashMap<>();
        users.put(1, new User("Alice"));
        users.put(2, new User("Bob"));
        Dao<User> userDao = new UserDao(users);
        userDao.findAll().forEach(System.out::println);
    }
}